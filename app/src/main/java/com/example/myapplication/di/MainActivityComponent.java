package com.example.myapplication.di;

import com.example.coremodule.di.CoreComponent;
import com.example.coremodule.di.scope.ActivityScope;
import com.example.myapplication.activity.MainActivity;

import dagger.Component;

@ActivityScope
@Component(modules = {MainActivityModule.class},
        dependencies = {CoreComponent.class})
public interface MainActivityComponent {

    void inject(MainActivity mainActivity);
}
