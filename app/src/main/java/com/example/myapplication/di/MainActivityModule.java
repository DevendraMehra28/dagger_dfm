package com.example.myapplication.di;

import com.example.coremodule.model.Car;

import dagger.Module;

@Module
public class MainActivityModule {

    Car car(Car car) {
        return car;
    }
}
