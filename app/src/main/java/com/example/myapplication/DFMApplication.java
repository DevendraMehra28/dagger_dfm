package com.example.myapplication;

import android.app.Application;

import com.example.coremodule.di.ContextModule;
import com.example.coremodule.di.CoreComponent;
import com.example.coremodule.di.CoreComponentProvider;
import com.example.coremodule.di.DaggerCoreComponent;

public class DFMApplication extends Application implements CoreComponentProvider {

    private CoreComponent coreComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        coreComponent = DaggerCoreComponent
                .builder()
                .contextModule(new ContextModule(this))
                .build();

    }

    @Override
    public CoreComponent getCoreComponent() {
        return coreComponent;
    }
}
