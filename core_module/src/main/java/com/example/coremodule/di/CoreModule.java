package com.example.coremodule.di;

import com.example.coremodule.model.Car;
import com.example.coremodule.model.Engine;
import com.example.coremodule.model.Wheel;

import dagger.Module;
import dagger.Provides;

@Module
public class CoreModule {
    @Provides
    Car provideCar(Engine engine, Wheel wheel) {
        return new Car(engine, wheel);
    }

}
