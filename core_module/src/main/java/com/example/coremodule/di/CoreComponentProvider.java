package com.example.coremodule.di;

public interface CoreComponentProvider {

    CoreComponent getCoreComponent();
}
