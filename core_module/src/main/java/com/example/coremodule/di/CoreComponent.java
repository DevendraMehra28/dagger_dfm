package com.example.coremodule.di;

import android.content.Context;

import com.example.coremodule.model.Car;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ContextModule.class, CoreModule.class})
public interface CoreComponent {


    Context getContext();

    Car getCar();
}
