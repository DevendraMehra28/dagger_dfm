package com.example.coremodule.di;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {

    private Application application;


    public ContextModule(Application application) {
        this.application = application;
    }

    @Singleton
    @Provides
    Context getContext() {
        return application;
    }
}
