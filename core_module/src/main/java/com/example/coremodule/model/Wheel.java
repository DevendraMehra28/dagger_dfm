package com.example.coremodule.model;

import javax.inject.Inject;

public class Wheel {

    @Inject
    public Wheel() {
    }

    public int getWheelCount() {
        return 4;
    }
}
