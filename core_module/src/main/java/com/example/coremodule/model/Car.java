package com.example.coremodule.model;

import javax.inject.Inject;

public class Car {

    private Wheel wheel;
    private Engine engine;

    @Inject
    public Car(Engine engine, Wheel wheel) {
        this.engine = engine;
        this.wheel = wheel;
    }

    public Wheel getWheel() {
        return wheel;
    }

    public Engine getEngine() {
        return engine;
    }
}
