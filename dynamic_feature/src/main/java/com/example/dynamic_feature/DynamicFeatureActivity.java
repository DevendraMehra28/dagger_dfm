package com.example.dynamic_feature;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class DynamicFeatureActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_feature);
    }
}
